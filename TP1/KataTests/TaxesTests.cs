﻿using System;
using Katas;
using Xunit;

namespace KataTests;

public class TaxesTests
{
    [Theory]
    [InlineData(-150)]
    public void ShouldThrowExceptionWhenAnnualIncomeIsNegative(double annualIncome)
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => new Taxes(annualIncome));
    }
    
    [Theory]
    [InlineData(5000, 0)]
    [InlineData(13000, 11)]
    [InlineData(50000, 30)]
    [InlineData(100000, 41)]
    [InlineData(170000, 45)]
    public void ShouldReturnCorrectTaxRate(double annualIncome, double expectedTaxRate)
    {
        var taxes = new Taxes(annualIncome);
        Assert.Equal(expectedTaxRate, taxes.TaxRate);
    }
    
    [Theory]
    [InlineData(5000, 0)]
    [InlineData(13000, 1430)]
    [InlineData(50000, 15000)]
    [InlineData(100000, 41000)]
    [InlineData(170000, 76500)]
    public void ShouldReturnCorrectAmountDue(double annualIncome, double expectedAmountDue)
    {
        var taxes = new Taxes(annualIncome);
        Assert.Equal(expectedAmountDue, taxes.AmountDue);
    }
}