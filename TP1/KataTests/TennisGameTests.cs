﻿using System;
using Katas;
using Xunit;

namespace KataTests;

public class TennisGameTest
{
    [Theory]
    [InlineData(-1, 0)]
    [InlineData(0, -1)]
    [InlineData(-2, -2)]
    public void ShouldThrowExceptionWhenExchangesAreNegative(int firstPlayerExchangesWon, int secondPlayerExchangesWon)
    {
        var game = new TennisGame(firstPlayerExchangesWon, secondPlayerExchangesWon);
        Assert.Throws<ArgumentOutOfRangeException>(() => game.ComputeScore());
    }
    
    [Theory]
    [InlineData(0, 0, 0, 0)]
    [InlineData(1, 0, 15, 0)]
    [InlineData(2, 0, 30, 0)]
    [InlineData(3, 0, 40, 0)]
    [InlineData(4, 2, 40, 30)]
    [InlineData(2, 3, 30, 40)]
    public void ShouldReturnCorrectScore(
        int firstPlayerExchangesWon,
        int secondPlayerExchangesWon,
        int firstPlayerExpectedScore,
        int secondPlayerExpectedScore
    )
    {
        var game = new TennisGame(firstPlayerExchangesWon, secondPlayerExchangesWon);
        game.ComputeScore();
        Assert.Equal(firstPlayerExpectedScore, game.FirstPlayerScore);
        Assert.Equal(secondPlayerExpectedScore, game.SecondPlayerScore);
    }
    
    [Theory]
    [InlineData(4, 0, TennisGame.FirstPlayer)]
    [InlineData(0, 4, TennisGame.SecondPlayer)]
    [InlineData(7, 5, TennisGame.FirstPlayer)]
    [InlineData(5, 7, TennisGame.SecondPlayer)]
    public void ShouldReturnCorrectWinner(int firstPlayerExchangesWon, int secondPlayerExchangesWon, string winner)
    {
       var game = new TennisGame(firstPlayerExchangesWon, secondPlayerExchangesWon);
       game.ComputeScore();
       Assert.Equal(winner, game.Winner);
    }
}