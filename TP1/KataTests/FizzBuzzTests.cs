using System;
using System.Linq;
using Katas;
using Xunit;

namespace KataTests;

public class FizzBuzzTests
{
    [Theory]
    [InlineData(0)]
    [InlineData(175)]
    public void ThrowsArgumentException(int n)
    {
        Assert.Throws<ArgumentException>(() => FizzBuzz.Generate(n));
    }
   
    [Theory]
    [InlineData(15, 4, 2, 1)]
    [InlineData(30, 8, 4, 2)]
    [InlineData(45, 12, 6, 3)]
    [InlineData(120, 32, 16, 8)]
    public void ReturnCorrectString(int n, int fizzExpected, int buzzExpected, int fizzBuzzExpected)
    {
        string fizzBuzz = FizzBuzz.Generate(n);
        int fizzCount = fizzBuzz.Split(" ").Count(s => s.Equals("Fizz"));
        int buzzCount = fizzBuzz.Split(" ").Count(s => s.Equals("Buzz"));
        int fizzBuzzCount = fizzBuzz.Split(" ").Count(s => s.Equals("FizzBuzz"));
        
        Assert.NotEmpty(fizzBuzz);
        Assert.Equal(fizzExpected, fizzCount);
        Assert.Equal(buzzExpected, buzzCount);
        Assert.Equal(fizzBuzzExpected, fizzBuzzCount);
    }

}