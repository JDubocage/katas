﻿namespace Katas;

public class FizzBuzz
{
    public static string Generate(int n)
    {
        if (n is < 15 or > 150)
        {
            throw new ArgumentException("n must be between 15 and 150", nameof(n));
        }

        string fizzBuzz = "";
        for (int i = 1; i <= n; i++)
        {
            if (i % 15 == 0)
            {
                fizzBuzz += "FizzBuzz ";
            }
            else if (i % 3 == 0)
            {
                fizzBuzz += "Fizz ";
            }
            else if (i % 5 == 0)
            {
                fizzBuzz += "Buzz ";
            }
            else
            {
                fizzBuzz += $"{i} ";
            }
        }
        
        return fizzBuzz;
    }
}