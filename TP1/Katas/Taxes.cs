﻿namespace Katas;

public class Taxes
{
    public double AnnualIncome { get; set; }
    public double TaxRate { get; private set; }
    public double AmountDue { get; private set; }
    
    public Taxes(double annualIncome)
    {
        AnnualIncome = annualIncome;
        Compute();
    }

    public void PrintResult()
    {
        Console.WriteLine($"Revenu annuel : {AnnualIncome}");
        Console.WriteLine($"Taux d'imposition : {TaxRate}%");
        Console.WriteLine($"Montant d'impôts à payer : {AmountDue}");
    }
    
    private void Compute()
    {
        CheckIfAnnualIncomeIsInRange(); 
        TaxRate = GetTaxRate();
        AmountDue = AnnualIncome * TaxRate / 100;
    }
    
    private void CheckIfAnnualIncomeIsInRange()
    {
        if (AnnualIncome < 0)
        {
            throw new ArgumentOutOfRangeException();
        }
    }

    private double GetTaxRate() => AnnualIncome switch
    {
        <= 10777 => 0,
        <= 27478 => 11,
        <= 78570 => 30,
        <= 168994 => 41,
        _ => 45
    };

}