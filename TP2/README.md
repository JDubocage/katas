## RealEstateCredit :
Projet API C# ASP.NET Core

## real-estate-credit-frontend
Projet front-end web en [SolidJS](https://www.solidjs.com/) contenant le formulaire de l'application.  
Environnement de développement local : Node + Vite  
Pour lancer l'application : `npm install` puis `npm start`