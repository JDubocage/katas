using System;
using RealEstateCreditDomain.Calculators;
using Xunit;

namespace RealEstateCreditTests;

public class MonthlyPaymentCalculatorTests
{
    private const double ValidBorrowedCapital = 30000;
    private const double ValidAnnualRate = 2.56;
    private const int ValidDuration = 15;
    
    [Theory]
    [InlineData(-5000)]
    [InlineData(0)]
    [InlineData(49999)]
    public void BorrowedCapitalShouldBeAtLeastFiftyThousand(double borrowedCapital)
    {
        Assert.Throws<ArgumentOutOfRangeException>(() =>
            new PaymentCalculator(borrowedCapital, ValidAnnualRate, ValidDuration));
    }

    [Theory]
    [InlineData(-5)]
    [InlineData(0)]
    [InlineData(11)]
    public void AnnualRateShouldBeBetweenZeroAndTen(double annualRate)
    {
        Assert.Throws<ArgumentOutOfRangeException>(() =>
            new PaymentCalculator(ValidBorrowedCapital, annualRate, ValidDuration));
    }
    
    [Theory]
    [InlineData(-5)]
    [InlineData(8)]
    [InlineData(26)]
    public void NumberOfYearsShouldBeBetweenNineAndTwentyFive(int numberOfYears)
    {
        Assert.Throws<ArgumentOutOfRangeException>(() =>
            new PaymentCalculator(ValidBorrowedCapital, ValidAnnualRate, numberOfYears));
    }

    [Theory]
    [InlineData(175000, 2.56, 25, 790.38)]
    [InlineData(75000, 2.63, 15, 504.69)]
    [InlineData(90000, 2.90, 12, 740.79)]
    public void ShouldReturnCorrectMonthlyPayment(
        double borrowedCapital,
        double annualRate,
        int numberOfYears,
        double expectedMonthlyPayment)
    {
        var paymentCalculator = new PaymentCalculator(borrowedCapital, annualRate, numberOfYears);
        Assert.Equal(expectedMonthlyPayment, paymentCalculator.CalculateMonthlyPayment());
    }
    
    [Theory]
    [InlineData(175000, 3.0, 25, 73961)]
    [InlineData(75000, 2.63, 15, 15844.2)]
    [InlineData(90000, 2.90, 12, 16673.76)]
    public void ShouldReturnCorrectTotalInterestFees(
        double borrowedCapital, 
        double annualRate,
        int numberOfYears,
        double expectedTotalInterestFees)
    {
        var paymentCalculator = new PaymentCalculator(borrowedCapital, annualRate, numberOfYears);
        Assert.Equal(expectedTotalInterestFees, paymentCalculator.CalculateTotalInterestFees());
    }
    
    [Theory]
    [InlineData(175000, 2.56, 25, 94845.6)]
    [InlineData(75000, 2.63, 15, 60562.8)]
    [InlineData(90000, 2.90, 12, 88894.8)]
    public void ShouldReturnCorrectCapitalAmountRepaidAfterTenYears(
        double borrowedCapital,
        double annualRate,
        int numberOfYears,
        double expectedCapitalAmountRepaidAfterTenYears)
    {
        var paymentCalculator = new PaymentCalculator(borrowedCapital, annualRate, numberOfYears);
        Assert.Equal(expectedCapitalAmountRepaidAfterTenYears,
            paymentCalculator.CalculateCapitalAmountRepaidAfterYears(10));
    }
}
