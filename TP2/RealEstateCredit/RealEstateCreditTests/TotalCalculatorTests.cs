﻿using RealEstateCreditDomain.Calculators;
using Xunit;

namespace RealEstateCreditTests;

public class TotalCalculatorTests
{
    [Theory]
    [InlineData(790.38, 100, 890.38)]
    [InlineData(504.69, 50, 554.69)]
    [InlineData(740.79, 89.56, 830.35)]
    public void ShouldReturnCorrectTotalMonthlyPayment(
        double monthlyPayment,
        double monthlyInsuranceFees,
        double expectedTotalMonthlyPayment)
    {
        Assert.Equal(expectedTotalMonthlyPayment,
            TotalCalculator.CalculateTotalMonthlyPayment(monthlyPayment, monthlyInsuranceFees));
    }
}