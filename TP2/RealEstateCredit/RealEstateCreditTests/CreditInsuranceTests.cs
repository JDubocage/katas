﻿using RealEstateCreditDomain.Calculators;
using Xunit;

namespace RealEstateCreditTests;

public class CreditInsuranceTests
{
   private const double ValidCapital = 100000;
   private const int ValidDuration = 12;
   
   [Theory]
   [InlineData(0.30, false, false, false, false, false)]
   [InlineData(0.25, true, false, false, false, false)]
   [InlineData(0.45, false, true, false, false, false)]
   [InlineData(0.60, false, false, true, false, false)]
   [InlineData(0.25, false, false, false, true, false)]
   [InlineData(0.45, false, false, false, false, true)]
   [InlineData(0.80, true, true, true, true, true)]
   public void ShouldReturnCorrectRate(
      double expectedRate,
      bool isSporty,
      bool isSmoker,
      bool isCardiac,
      bool isComputerEngineer,
      bool isPilot)
   {
      var creditInsuranceCalculator = new CreditInsuranceCalculator(ValidCapital, ValidDuration);
      var rate = creditInsuranceCalculator.CalculateRate(isSporty, isSmoker, isCardiac, isComputerEngineer, isPilot);
      Assert.Equal(expectedRate, rate);
   }
   
   [Theory]
   [InlineData(12.5, 50000, 0.3)]
   [InlineData(15.62, 75000, 0.25)]
   [InlineData(37.5, 100000, 0.45)]
   [InlineData(133.33, 200000, 0.8)]
   public void ShouldReturnCorrectMonthlyFee(double expectedMonthlyFee, double borrowedCapital, double insuranceRate)
   {
      var creditInsuranceCalculator = new CreditInsuranceCalculator(borrowedCapital, ValidDuration);
      var monthlyFee = creditInsuranceCalculator.CalculateMonthlyFee(insuranceRate);
      Assert.Equal(expectedMonthlyFee, monthlyFee);
   }
   
   [Theory]
   [InlineData(30624, 175000, 25, 0.7)]
   [InlineData(3600, 100000, 12, 0.3)]
   [InlineData(18000, 200000, 20, 0.45)]
   public void ShouldReturnCorrectTotalFee(
      double expectedTotalFee, 
      double borrowedCapital,
      int numberOfYears,
      double insuranceRate)
   {
      var creditInsuranceCalculator = new CreditInsuranceCalculator(borrowedCapital, numberOfYears);
      var totalFee = creditInsuranceCalculator.CalculateTotalFee(insuranceRate);
      Assert.Equal(expectedTotalFee, totalFee);
   }
}