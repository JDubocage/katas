﻿using RealEstateCreditDomain.Records;

namespace RealEstateCreditDomain.Calculators;

public class PaymentCalculator
{
    private BorrowedCapital Capital { get; }
    private Rate AnnualRate { get; }
    private NumberOfYears Duration { get; }
    
    public PaymentCalculator(BorrowedCapital capital, Rate annualRate, NumberOfYears duration)
    {
        Capital = capital;
        AnnualRate = annualRate;
        Duration = duration;
    }

    public double CalculateMonthlyPayment()
    {
        var nominator = Capital * AnnualRate.ToNumericRate() / 12;
        var denominator = 1 - Math.Pow(1 + AnnualRate.ToNumericRate() / 12, -Duration.ToNumberOfMonths());
        return Math.Round(nominator / denominator, 2);
    }

    public double CalculateTotalInterestFees()
    {
        var monthlyPayment = CalculateMonthlyPayment();
        var totalInterestFees = monthlyPayment * Duration.ToNumberOfMonths() - Capital;
        return Math.Round(totalInterestFees, 2);
    }
    
    public double CalculateCapitalAmountRepaidAfterYears(NumberOfYears numberOfYears)
    {
        var monthlyPayment = CalculateMonthlyPayment();
        var capitalAmountRepaid = monthlyPayment * numberOfYears.ToNumberOfMonths();
        return Math.Round(capitalAmountRepaid, 2);
    }
}
