﻿using RealEstateCreditDomain.Records;

namespace RealEstateCreditDomain.Calculators;

public class CreditInsuranceCalculator
{
    private const double BaseRate = 0.3;

    public CreditInsuranceCalculator(BorrowedCapital capital, NumberOfYears duration)
    {
        Capital = capital;
        Duration = duration;
    }

    private BorrowedCapital Capital { get; }
    private NumberOfYears Duration { get; }
    
    public double CalculateRate(
        bool isSporty,
        bool isSmoker,
        bool isCardiac,
        bool isComputerEngineer, 
        bool isPilot)
    {
        double rate = BaseRate;
        
        if (isSporty)
            rate -= 0.05;
        if (isSmoker)
            rate += 0.15;
        if (isCardiac)
            rate += 0.3;
        if (isComputerEngineer)
            rate -= 0.05;
        if (isPilot)
            rate += 0.15;

        return Math.Round(rate, 2);
    }

    public double CalculateMonthlyFee(Rate insuranceRate)
    {
        return Math.Round(Capital * insuranceRate.ToNumericRate() / 12, 2);
    }
    
    public double CalculateTotalFee(Rate insuranceRate)
    {
        return Math.Round(CalculateMonthlyFee(insuranceRate) * Duration * 12, 2);
    }
}