﻿namespace RealEstateCreditDomain.Calculators;

public static class TotalCalculator
{
    public static double CalculateTotalMonthlyPayment(double monthlyPayment, double monthlyInsuranceFees)
    {
        return Math.Round(monthlyPayment + monthlyInsuranceFees, 2);
    }
}