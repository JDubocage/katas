﻿namespace RealEstateCreditDomain.Records;

public record NumberOfYears
{
    private int Duration { get; }
    
    private NumberOfYears(int duration)
    {
        if (duration is < 9 or > 25)
        {
            throw new ArgumentOutOfRangeException(nameof(duration),
                "La durée du prêt doit être comprise entre 9 et 25 ans");
        }
        Duration = duration;
    }
    
    public int ToNumberOfMonths() => Duration * 12; 
    public static implicit operator int(NumberOfYears numberOfYears) => numberOfYears.Duration;
    public static implicit operator NumberOfYears(int duration) => new(duration);
}
