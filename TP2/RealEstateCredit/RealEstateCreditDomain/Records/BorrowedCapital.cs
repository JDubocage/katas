﻿namespace RealEstateCreditDomain.Records;

public record BorrowedCapital
{
    private double Amount { get; }

    private BorrowedCapital(double amount)
    {
        if (amount < 50000)
        {
            throw new ArgumentOutOfRangeException(nameof(amount), 
                "Le montant emprunté doit être supérieur à 50000€");
        }
        Amount = amount;
    }
    
    public static implicit operator double(BorrowedCapital borrowedCapital) => borrowedCapital.Amount;
    public static implicit operator BorrowedCapital(double amount) => new(amount);
};
