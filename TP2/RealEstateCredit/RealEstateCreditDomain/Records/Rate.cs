﻿namespace RealEstateCreditDomain.Records;

public record Rate
{
    private double Value { get; }

    private Rate(double value)
    {
        if (value is < 0 or > 10)
        {
            throw new ArgumentOutOfRangeException(nameof(value), 
                "Le taux doit être compris entre 0 et 10%");
        }
        Value = value;
    }
    
    public double ToNumericRate() => Value / 100;
    public static implicit operator double(Rate rate) => rate.Value;
    public static implicit operator Rate(double value) => new(value);
};
