﻿using Microsoft.AspNetCore.Mvc;
using RealEstateCredit.Handlers;
using RealEstateCredit.Models;

namespace RealEstateCredit.Controllers;

[ApiController]
[Route("realEstateCredit")]
public class RealEstateCreditController : ControllerBase
{
   [HttpPost("estimate")]
   public IActionResult Post([FromBody] RealEstateEstimateRequest request)
   {
      try
      {
         var response = RealEstateCreditHandler.Handle(request); 
         return Ok(response);
      }
      catch (Exception exception)
      {
         var response = new ExceptionResponse {ExceptionMessage = exception.Message};
         return BadRequest(response);
      }
   }
}
