﻿namespace RealEstateCredit.Models;

public class ExceptionResponse
{
    public string? ExceptionMessage { get; set; }
}
