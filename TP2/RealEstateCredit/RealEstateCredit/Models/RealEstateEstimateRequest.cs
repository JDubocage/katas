﻿namespace RealEstateCredit.Models;

public class RealEstateEstimateRequest
{
    public double BorrowedCapital { get; set; }
    public double AnnualRate { get; set; }
    public int NumberOfYears { get; set; }
    public bool IsSporty { get; set; }
    public bool IsSmoker { get; set; }
    public bool IsCardiac { get; set; }
    public bool IsComputerEngineer { get; set; }
    public bool IsPilot { get; set; }
}