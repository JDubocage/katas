﻿namespace RealEstateCredit.Models;

public class RealEstateEstimateResponse
{
    public double TotalMonthlyPayment { get; set; }
    public double TotalInterestFees { get; set; }
    public double MonthlyInsuranceFees { get; set; }
    public double TotalInsuranceFees { get; set; }
    public double CapitalAmountRepaidAfterTenYears { get; set; }
}