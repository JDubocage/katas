﻿using RealEstateCredit.Models;
using RealEstateCreditDomain.Calculators;

namespace RealEstateCredit.Handlers;

public static class RealEstateCreditHandler
{
    public static RealEstateEstimateResponse Handle(RealEstateEstimateRequest request)
    {
        var paymentCalculator =
            new PaymentCalculator(request.BorrowedCapital, request.AnnualRate, request.NumberOfYears);
         
        var monthlyPayment = paymentCalculator.CalculateMonthlyPayment();
        var totalInterestFees = paymentCalculator.CalculateTotalInterestFees();
        var capitalAmountRepaidAfterTenYears = paymentCalculator.CalculateCapitalAmountRepaidAfterYears(10);
         
        var insuranceCalculator = new CreditInsuranceCalculator(request.BorrowedCapital, request.NumberOfYears);
        var insuranceRate = insuranceCalculator.CalculateRate(
            request.IsSporty,
            request.IsSmoker,
            request.IsCardiac,
            request.IsComputerEngineer,
            request.IsPilot);
         
        var monthlyInsuranceFees = insuranceCalculator.CalculateMonthlyFee(insuranceRate);
        var totalInsuranceFees = insuranceCalculator.CalculateTotalFee(insuranceRate);
        var totalMonthlyPayment = TotalCalculator.CalculateTotalMonthlyPayment(monthlyPayment, monthlyInsuranceFees);
         
        return new RealEstateEstimateResponse
        {
            TotalMonthlyPayment = totalMonthlyPayment,
            TotalInterestFees = totalInterestFees,
            MonthlyInsuranceFees = monthlyInsuranceFees,
            TotalInsuranceFees = totalInsuranceFees,
            CapitalAmountRepaidAfterTenYears = capitalAmountRepaidAfterTenYears
        };
    }
}