export type Response = {
    success: boolean;
    exceptionMessage: string;
}