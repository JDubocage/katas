export type RealEstateEstimate = {
    totalMonthlyPayment: number;
    totalInterestFees: number;
    monthlyInsuranceFees: number;
    totalInsuranceFees: number;
    capitalAmountRepaidAfterTenYears: number;
}


