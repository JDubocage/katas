import type { Component } from 'solid-js';
import { createForm, Field, Form } from '@modular-forms/solid';
import { Show } from "solid-js";
import axios, { AxiosError } from 'axios';
import { RealEstateEstimate } from "../types/realEstateEstimate";
import { createStore } from "solid-js/store";
import { Response } from "../types/response";

type RealEstateCreditForm = {
    borrowedCapital: number;
    annualRate: number;
    numberOfYears: number;
    isSporty: boolean;
    isSmoker: boolean;
    isCardiac: boolean;
    isComputerEngineer: boolean;
    isPilot: boolean;
}

const RealEstateForm: Component = () => {
    const realEstateForm = createForm<RealEstateCreditForm>();
    const [response, setResponse] = createStore<Response>({success: true, exceptionMessage: ""});
    const [realEstateEstimate, setRealEstateEstimate] = createStore<RealEstateEstimate>(
        {
            totalMonthlyPayment: 0,
            totalInterestFees: 0,
            monthlyInsuranceFees: 0,
            totalInsuranceFees: 0,
            capitalAmountRepaidAfterTenYears: 0
        });

    const handlePost = async (values: RealEstateCreditForm) => {
        try {
            const response = await axios.post('https://localhost:7097/realEstateCredit/estimate', values);
            setRealEstateEstimate(response.data);
            setResponse({success: true, exceptionMessage: ""});
        } catch (error: unknown) {
            if (error instanceof AxiosError && error.response != undefined && error.response.status == 400) {
                setResponse({success: false, exceptionMessage: error.response.data.exceptionMessage});
            } else {
                console.log(error);
            }
        }
    };

    return (
        <div class="content">
            <Form of={realEstateForm} onSubmit={handlePost}>
                <label for="borrowedCapital">Capital Emprunté</label>
                <Field of={realEstateForm} name="borrowedCapital">
                    {(field => <input type="number" {...field.props} />)}
                </Field>
                <label for="annualRate">Taux Annuel</label>
                <Field of={realEstateForm} name="annualRate">
                    {(field => <input type="number" {...field.props} />)}
                </Field>
                <label for="numberOfYears">Nombre d'Années</label>
                <Field of={realEstateForm} name="numberOfYears">
                    {(field => <input type="number" {...field.props} />)}
                </Field>
                <div>
                    <Field of={realEstateForm} name="isSporty">
                        {(field => <input type="checkbox" {...field.props} />)}
                    </Field>
                    <label for="isSporty">Je suis sportif</label>
                </div>
                <div>
                    <Field of={realEstateForm} name="isSmoker">
                        {(field => <input type="checkbox" {...field.props} />)}
                    </Field>
                    <label for="isSmoker">Je suis fumeur</label>
                </div>
                <div>
                    <Field of={realEstateForm} name="isCardiac">
                        {(field => <input type="checkbox" {...field.props} />)}
                    </Field>
                    <label for="isCardiac">Je suis sujet à des troubles cardiaques</label>
                </div>
                <div>
                    <Field of={realEstateForm} name="isComputerEngineer">
                        {(field => <input type="checkbox" {...field.props} />)}
                    </Field>
                    <label for="isComputerEngineer">Je suis ingénieur en informatique</label>
                </div>
                <div>
                    <Field of={realEstateForm} name="isPilot">
                        {(field => <input type="checkbox" {...field.props} />)}
                    </Field>
                    <label for="isPilot">Je suis pilote de chasse</label>
                </div>
                <input type="submit"/>
            </Form>
            <Show
                when={response.success}
                fallback={<p style={{color: 'red'}}> Erreur : {response.exceptionMessage} </p>}>
                <p> Montant total de la mensualité: {realEstateEstimate.totalMonthlyPayment} </p>
                <p> Montant de la cotisation mensuelle d’assurance : {realEstateEstimate.monthlyInsuranceFees} </p>
                <p> Montant total des intêrets : {realEstateEstimate.totalInterestFees} </p>
                <p> Montant total de l'assurance : {realEstateEstimate.totalInsuranceFees} </p>
                <p> Montant du capital remboursé au bout de 10 ans : {realEstateEstimate.capitalAmountRepaidAfterTenYears } </p>
            </Show>
        </div>
    );
}

export default RealEstateForm;