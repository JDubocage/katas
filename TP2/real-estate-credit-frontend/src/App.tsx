import type { Component } from 'solid-js';
import RealEstateForm from "./components/RealEstateForm";

const App: Component = () => {
    return (
        <RealEstateForm/>
    );
};

export default App;
